package edu.ucsd.cs.joseph.xml.tutorial.parse;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestXMLParseWithDom {
    private static final Logger logger = LoggerFactory.getLogger(TestXMLParseWithDom.class);

    @Test
    public void testSimplestXML() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            StringBuilder sb = new StringBuilder();
            sb.append("<?xml version=\"1.0\"?> <class>Hello World!</class>");
            ByteArrayInputStream input = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);

            Element root = doc.getDocumentElement();
            assertFalse(root == null);

            NodeList children = doc.getChildNodes();
            assertTrue(children.getLength() == 1);

            for(int i = 0; i < children.getLength(); i++) {
                logger.info("Child " + i + ": " + children.item(i).getNodeName() + "-" + children.item(i).getTextContent());
            }
        }
        catch (ParserConfigurationException e) {
            fail();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (SAXException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testParseXMLFromFile() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(getClass().getResource("/j_caesar.xml").getFile());
            Document doc = builder.parse(file);

            Element root = doc.getDocumentElement();
            assertFalse(root == null);

            NodeList children = doc.getChildNodes();
//            assertTrue(children.getLength() == 3);
            logger.info("children number" + children.getLength());
            for(int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                logger.info("Child " + i + ": " + child.getNodeName() + "-" + child.getTextContent());

                NodeList children2 = child.getChildNodes();
//                for(int j = 0; j < children2.getLength(); j++) {
//
//                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
