package edu.ucsd.cs.joseph.antlr4.tutorial;

import org.antlr.v4.runtime.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestArithmeticParser {
    public static final Logger logger = LoggerFactory.getLogger(TestArithmeticParser.class);

    public static final String[] validStrings = {
            "1.2",
            "1+1",
            "1+1+2*3+4/6",
            "1 + 1",
            "1 +1+1",
            "3 * 4",
            "1 + 3 * 4",
            "3 /4+1",
            "3 * (1 + 4 + 5 * 3)",
            "1 + (30 - (5 - (2 + (4 - 5))))",
            "((2))",
            "((((4))))",
            "(1) - (2)",
            "(1 + 1)",
            "(2) + (3 - 5) * (3-2)",
            "3 * 2 / 3 * 4",
            "3 + 2 - 5 + 2",
            "((((1 + 30) - 5) - 2) + 4) - 5",
            "2 * ((((3 + 4) -3)+ 7) - 5)",
            "1 + 1 + 1 - 1 + 1 + 1 - 1",
            "2 + 3 * 2 / 7 * 2 / 4 - 2 - 1 + 5",
            "3 * (1 + 1 + 1 - (3+4-2-2-2)) / 4",
            "(1 - 4) * (2 - 3) / (3 * (4-7))",
            "1 + 1 + 1 * 2 * (4+2) * 2 - (1 + 1 - 4 + 1 +1 ) * 2 / 3 / 3 / 3"
    };

    public static Double[] results = {
            1.2,
            2D,
            8.666666666667,
            2D,
            3D,
            12D,
            13D,
            1.75,
            60D,
            27D,
            2D,
            4D,
            -1D,
            2D,
            0D,
            8.0,
            2D,
            23D,
            12D,
            3D,
            4.4285714285714285714285714285714,
            1.5,
            -0.333333333333333333334,
            26D
    };

    public static final String[] invalidStrings = {
            "1.",
            "1+1-",
            "1+1+2*3+4//6",
            "1 ++ 1",
            "1 +* 1 + 1",
            "3 * 4.4.4",
            "1 + 3 * 4.4.",
            "3 /)(4+1",
            "3 * ((1 + 4 + 5 * 3)",
            "1 + (30 -+ (5 - (2 + (4 - 5))))",
            "((2)))",
            "((((4)))))",
            "(1) - (2))",
            "(1 + 1()",
            "(2) + ()3 - 5) * (3-2)"
    };

    @Test
    public void testProgram() throws IOException {
        for(String program : validStrings) {
            assertTrue(isValidProgram(program));
        }
    }

    @Test
    public void testInvalidProgram() throws IOException {
        for(String program : invalidStrings) {
            assertFalse("program '" + program + "' is invalid", isValidProgram(program));
        }
    }

    @Test
    public void testParseTreeToString() throws IOException {
        String program = "1 + (30 / (3 * 4.4) + (5.34 * 0.3 - (0.2 + (4 - 5))))";
        ArithmeticTestErrorListener errorListener = new ArithmeticTestErrorListener();
        ArithmeticParser.ProgramContext context = parse(program, errorListener);
        assertFalse(errorListener.isFail());
        ArithmeticVisitor<String> dumper = new ParseTreeDumperVisitor();
        String result = dumper.visit(context);
        logger.info(result);
    }

    @Test
    public void testNaiveInterpreterVisitor() throws IOException {
        String program = "2 + 3 * 4";
        Double result = interpret(program);
        Double epsilon = Double.valueOf("0.00001");
        logger.info("result=" + result);
        assertTrue(Math.abs(result - (double)14) <= epsilon);
    }

    @Test
    public void testNaiveInterpreterVisitorBatch() throws IOException {
        int index = 0;
        Double epsilon = Double.valueOf("0.00001");
        for(Double expected : results) {
            String program = validStrings[index];
            Double result = interpret(program);

            ParseTreeDumperVisitor dumper = new ParseTreeDumperVisitor();
            ArithmeticParser.ProgramContext conext = parse(program, new ArithmeticTestErrorListener());
            String tree = dumper.visit(conext);
            logger.debug(tree);

            assertTrue("Result doesn't match for the program '" + program + "'. Expected " + expected
                        + " but got " + result + System.lineSeparator() + "parse tree: " + System.lineSeparator()
                        + tree, Math.abs(result - expected) <= epsilon);
            index++;
        }
    }

    private boolean isValidProgram(String program) throws IOException {
        ArithmeticTestErrorListener errorListener = new ArithmeticTestErrorListener();
        ArithmeticParser.ProgramContext context = parse(program, errorListener);
        return !errorListener.isFail();
    }

    public static ArithmeticParser.ProgramContext parse(String program, ANTLRErrorListener errorListener) throws IOException {
        CharStream input = new ANTLRInputStream(new StringReader(program));
        TokenSource source = new ArithmeticLexer(input);
        TokenStream tokenStream = new CommonTokenStream(source);
        ArithmeticParser parser = new ArithmeticParser(tokenStream);
        parser.addErrorListener(errorListener);
        return parser.program();
    }

    private Double interpret(String program) throws IOException {
        NaiveInterpreterVisitor visitor = new NaiveInterpreterVisitor();
        ArithmeticTestErrorListener errorListener = new ArithmeticTestErrorListener();
        ArithmeticParser.ProgramContext context = parse(program, errorListener);
        assertFalse(errorListener.isFail());
        return visitor.visit(context);
    }
}
