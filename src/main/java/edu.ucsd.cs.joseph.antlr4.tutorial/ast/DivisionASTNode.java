package edu.ucsd.cs.joseph.antlr4.tutorial.ast;

import edu.ucsd.cs.joseph.antlr4.tutorial.ast.visitor.ArithmeticASTVisitor;

public class DivisionASTNode extends BinaryExpression{
    public DivisionASTNode(ArithmeticASTNode left, ArithmeticASTNode right) {
        super();
        setLeft(left);
        setRight(right);
    }

    public Object accept(ArithmeticASTVisitor visitor) {
        return visitor.visit(this);
    }
}
