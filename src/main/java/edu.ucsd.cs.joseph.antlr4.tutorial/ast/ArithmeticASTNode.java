package edu.ucsd.cs.joseph.antlr4.tutorial.ast;

import edu.ucsd.cs.joseph.antlr4.tutorial.ast.visitor.ArithmeticASTVisitor;

public abstract class ArithmeticASTNode {
    public abstract Object accept(ArithmeticASTVisitor visitor);
}
