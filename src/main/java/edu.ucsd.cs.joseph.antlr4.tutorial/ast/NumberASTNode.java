package edu.ucsd.cs.joseph.antlr4.tutorial.ast;

import edu.ucsd.cs.joseph.antlr4.tutorial.ast.visitor.ArithmeticASTVisitor;

public class NumberASTNode extends ArithmeticASTNode{
    private Number value;

    public NumberASTNode(Number value) {
        super();
        this.value = value;
    }

    public Number getValue() {
        return value;
    }

    public void setValue(Number value) {
        this.value = value;
    }

    public Object accept(ArithmeticASTVisitor visitor) {
        return visitor.visit(this);
    }
}
