package edu.ucsd.cs.joseph.antlr4.tutorial.ast;

public abstract class BinaryExpression extends ArithmeticASTNode {
    private ArithmeticASTNode left;
    private ArithmeticASTNode right;

    public ArithmeticASTNode getLeft() {
        return left;
    }

    public void setLeft(ArithmeticASTNode left) {
        this.left = left;
    }

    public ArithmeticASTNode getRight() {
        return right;
    }

    public void setRight(ArithmeticASTNode right) {
        this.right = right;
    }
}
