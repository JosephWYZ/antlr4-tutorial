package edu.ucsd.cs.joseph.antlr4.tutorial.ast.visitor;

import edu.ucsd.cs.joseph.antlr4.tutorial.ast.*;

public class EvaluationVisitor implements ArithmeticASTVisitor {
    public Number visit(NumberASTNode node) {
        return node.getValue();
    }

    public Number visit(SumASTNode node) {
        Number left = (Number) node.getLeft().accept(this);
        Number right = (Number) node.getRight().accept(this);
        return left.doubleValue() + right.doubleValue();
    }

    public Number visit(DifferenceASTNode node) {
        Number left = (Number) node.getLeft().accept(this);
        Number right = (Number) node.getRight().accept(this);
        return left.doubleValue() - right.doubleValue();
    }

    public Number visit(MultiplicationASTNode node) {
        Number left = (Number) node.getLeft().accept(this);
        Number right = (Number) node.getRight().accept(this);
        return left.doubleValue() * right.doubleValue();
    }

    public Number visit(DivisionASTNode node) {
        Number left = (Number) node.getLeft().accept(this);
        Number right = (Number) node.getRight().accept(this);
        return left.doubleValue() / right.doubleValue();
    }
}
