package edu.ucsd.cs.joseph.antlr4.tutorial.ast.visitor;

import edu.ucsd.cs.joseph.antlr4.tutorial.ast.*;

public interface ArithmeticASTVisitor {
    Number visit(NumberASTNode node);

    Number visit(SumASTNode node);

    Number visit(DifferenceASTNode node);

    Number visit(MultiplicationASTNode node);

    Number visit(DivisionASTNode node);
}
