package edu.ucsd.cs.joseph.antlr4.tutorial;

import org.antlr.v4.runtime.tree.*;

public class ParseTreeDumperVisitor implements ArithmeticVisitor<String> {
    private int indentationLevel = 0;

    private void increase() {
        indentationLevel += 1;
    }

    private void decrease() {
        indentationLevel -= 1;
    }

    private String getIndentation() {
        StringBuilder temp = new StringBuilder();
        for(int i = 0; i < indentationLevel; i++) {
            temp.append("--");
        }
        return temp.toString();
    }

    public String visitProgram(ArithmeticParser.ProgramContext ctx) {
        return getIndentation() + visitChildren(ctx) + System.lineSeparator();
    }

    public String visitAlgebraicSum(ArithmeticParser.AlgebraicSumContext ctx) {
        return getIndentation() + "AlgebraicSum" + visitChildren(ctx);
    }

    public String visitMultiplication(ArithmeticParser.MultiplicationContext ctx) {
        return getIndentation() + "Multiplication" + visitChildren(ctx);
    }

    public String visitAtomicTerm(ArithmeticParser.AtomicTermContext ctx) {
        return getIndentation() + "AtomicTerm" + visitChildren(ctx);
    }

    public String visitNumber(ArithmeticParser.NumberContext ctx) {
        return getIndentation() + "Number: " + ctx.realNumber().accept(this);
    }

    public String visitInnerExpression(ArithmeticParser.InnerExpressionContext ctx) {
        return getIndentation() + "InnerExpression" + visitChildren(ctx);
    }

    public String visitRealNumber(ArithmeticParser.RealNumberContext ctx) {
        StringBuilder temp = new StringBuilder();
        temp.append(ctx.NUMBER(0).getText());
        if(ctx.getChildCount() > 2) {
            temp.append(".").append(ctx.NUMBER(1).getText());
        }
        temp.append(System.lineSeparator());
        return temp.toString();
    }

    public String visit(ParseTree parseTree) {
        return parseTree.accept(this);
    }

    public String visitChildren(RuleNode ruleNode) {
        StringBuilder temp = new StringBuilder();
        temp.append(System.lineSeparator());
        int childCount = ruleNode.getChildCount();
        increase();
        for(int i = 0; i < childCount; i++) {
            temp.append(ruleNode.getChild(i).accept(this));
        }
        decrease();
        return temp.toString();
    }

    public String visitTerminal(TerminalNode terminalNode) {
        return getIndentation() + "TERM: " + terminalNode.getText() + System.lineSeparator();
    }

    public String visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
