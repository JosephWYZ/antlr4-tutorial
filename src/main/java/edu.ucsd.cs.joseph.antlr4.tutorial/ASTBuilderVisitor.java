package edu.ucsd.cs.joseph.antlr4.tutorial;

import edu.ucsd.cs.joseph.antlr4.tutorial.ast.*;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ASTBuilderVisitor implements ArithmeticVisitor<ArithmeticASTNode> {
    private static final Logger logger = LoggerFactory.getLogger(ASTBuilderVisitor.class);

    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String TIMES = "*";
    public static final String DIVIDED = "/";

    public ArithmeticASTNode visitProgram(ArithmeticParser.ProgramContext ctx) {
        return ctx.expression().accept(this);
    }

    public ArithmeticASTNode visitAlgebraicSum(ArithmeticParser.AlgebraicSumContext ctx) {
        String operand = ctx.getChild(1).getText();
        ArithmeticASTNode left = ctx.expression(0).accept(this);
        ArithmeticASTNode right = ctx.expression(1).accept(this);
        if(operand.equals(PLUS)) {
            return new SumASTNode(left, right);
        }
        else if(operand.equals(MINUS)) {
            return new DifferenceASTNode(left, right);
        }
        else {
            throw new ArithmeticException("Something has really gone wrong: operand '" +
                    operand + "' comes as a complete surprise");
        }
    }

    public ArithmeticASTNode visitMultiplication(ArithmeticParser.MultiplicationContext ctx) {
        String operand = ctx.getChild(1).getText();
        ArithmeticASTNode left = ctx.expression(0).accept(this);
        ArithmeticASTNode right = ctx.expression(1).accept(this);
        if(operand.equals(TIMES)) {
            return new MultiplicationASTNode(left, right);
        }
        else if(operand.equals(DIVIDED)) {
            return new DivisionASTNode(left, right);
        }
        else {
            throw new ArithmeticException("Something has really gone wrong: operand '" +
                    operand + "' comes as a complete surprise");
        }
    }

    public ArithmeticASTNode visitAtomicTerm(ArithmeticParser.AtomicTermContext ctx) {
        return ctx.term().accept(this);
    }

    public ArithmeticASTNode visitNumber(ArithmeticParser.NumberContext ctx) {
        return ctx.realNumber().accept(this);
    }

    public ArithmeticASTNode visitInnerExpression(ArithmeticParser.InnerExpressionContext ctx) {
        return ctx.expression().accept(this);
    }

    public ArithmeticASTNode visitRealNumber(ArithmeticParser.RealNumberContext ctx) {
        StringBuilder number = new StringBuilder();
        number.append(ctx.NUMBER(0).getText());
        if(ctx.getChildCount() > 2) {
            number.append('.').append(ctx.NUMBER(1).getText());
        }
        return new NumberASTNode(Double.parseDouble(number.toString()));
    }

    public ArithmeticASTNode visit(ParseTree parseTree) {
        return parseTree.accept(this);
    }

    public ArithmeticASTNode visitChildren(RuleNode ruleNode) {
        return null;
    }

    public ArithmeticASTNode visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    public ArithmeticASTNode visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
