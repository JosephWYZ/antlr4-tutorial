package edu.ucsd.cs.joseph.antlr4.tutorial;

import org.antlr.v4.runtime.tree.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NaiveInterpreterVisitor implements ArithmeticVisitor<Double> {
    private static final Logger loger = LoggerFactory.getLogger(NaiveInterpreterVisitor.class);

    private ParseTreeProperty<Double> numberNodesAnnotations = new ParseTreeProperty<Double>();

    protected ParseTreeProperty<Double> getNumberNodesAnnotations() {
        return this.numberNodesAnnotations;
    }

    protected void setNumberNodesAnnotations(ParseTreeProperty<Double> numberNodesAnnotations) {
        this.numberNodesAnnotations = numberNodesAnnotations;
    }

    public Double visitProgram(ArithmeticParser.ProgramContext ctx) {
        return ctx.expression().accept(this);
    }

    public Double visitAlgebraicSum(ArithmeticParser.AlgebraicSumContext ctx) {
        String operand = ctx.getChild(1).getText();
        Double left = ctx.expression(0).accept(this);
        Double right = ctx.expression(1).accept(this);
        if(operand.equals("+")) {
            return left + right;
        }
        else if(operand.equals("-")) {
            return left - right;
        }
        else {
            throw new ArithmeticException("Something has really gone wrong");
        }
    }

    public Double visitMultiplication(ArithmeticParser.MultiplicationContext ctx) {
        String operand = ctx.getChild(1).getText();
        Double left = ctx.expression(0).accept(this);
        Double right = ctx.expression(1).accept(this);
        if(operand.equals("*")) {
            return left * right;
        }
        else if(operand.equals("/")) {
            return left / right;
        }
        else {
            throw new ArithmeticException("Something has really gone wrong");
        }
    }

    public Double visitAtomicTerm(ArithmeticParser.AtomicTermContext ctx) {
        return ctx.term().accept(this);
    }

    public Double visitNumber(ArithmeticParser.NumberContext ctx) {
        return ctx.realNumber().accept(this);
    }

    public Double visitInnerExpression(ArithmeticParser.InnerExpressionContext ctx) {
        return ctx.expression().accept(this);
    }

    public Double visitRealNumber(ArithmeticParser.RealNumberContext ctx) {
        String intPart = ctx.NUMBER(0).getText();
        if(ctx.getChildCount() == 3) {
            String decimalPart = ctx.NUMBER(1).getText();
            return Double.parseDouble(intPart + '.' + decimalPart);
        }
        else {
            return Double.parseDouble(intPart);
        }
    }

    public Double visit(ParseTree parseTree) {
        return parseTree.accept(this);
    }

    public Double visitChildren(RuleNode ruleNode) {
        return null;
    }

    public Double visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    public Double visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
